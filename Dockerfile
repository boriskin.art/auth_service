# Build binary with downloaded go modules
FROM golang:1.16.7-buster as build

# Copy files
RUN mkdir /home/auth_service
COPY ./go.mod /home/auth_service
WORKDIR /home/auth_service

# Download go modules to cache
RUN go mod download
COPY . /home/auth_service

# Build application
RUN CGO_ENABLED=0 go build -o auth_service ./cmd/auth_service/*

# Start app in low-size alpine image
FROM alpine:3.14.1

RUN mkdir auth_service

# Copy files from build step
WORKDIR auth_service
COPY --from=build /home/auth_service/auth_service .
RUN mkdir configs
COPY --from=build /home/auth_service/config ./config

# Start app
CMD ["./auth_service"]

