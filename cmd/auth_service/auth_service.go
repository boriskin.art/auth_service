package main

import "avito_service/internal/auth_service"

func main() {
	auth_service.Run()
}