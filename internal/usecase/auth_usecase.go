package usecase

import (
	"avito_service/internal/domain"
	"avito_service/pkg/passwords"
	"crypto/rand"
	"database/sql"
	"fmt"
)

type UserUsecase struct {
	userRepository domain.UserRepository
}

func NewUserUsecase(repository domain.UserRepository) *UserUsecase {
	return &UserUsecase{userRepository: repository}
}

func (uu *UserUsecase) SignUp(email, password string) (int, *domain.AuthError) {
	userID, err := uu.CheckUserExistence(email)

	if err != nil {
		return 0, domain.NewAuthError(300, "database error: " + err.Error())
	}
	if userID != 0 {
		return userID, domain.UserAlreadyExist
	}

	salt := make([]byte, 8)
	rand.Read(salt)
	hashedPassword := passwords.GeneratePasswordHash(fmt.Sprintf("%x", salt), password)

	userID, err = uu.userRepository.Create(domain.User{
		Email:    email,
		Password: hashedPassword,
	})
	if err != nil {
		return 0, domain.NewAuthError(300, "database error: " + err.Error())
	}

	return userID, nil
}

func (uu *UserUsecase) SignIn(email, password string) (int, *domain.AuthError) {
	user, err := uu.userRepository.Get(domain.User{
		Email:    email,
	})
	if err == sql.ErrNoRows {
		return 0, domain.UserDoesNotExist
	}
	if err != nil {
		return 0, domain.NewAuthError(300, "database error: " + err.Error())
	}

	// Password incorrect
	if !passwords.CheckPasswordHash(password, user.Password) {
		return 0, domain.UserDataIncorrect
	}
	
	return user.ID, nil
}

func (uu *UserUsecase) CheckUserExistence(email string) (int, error) {
	user, err := uu.userRepository.Get(domain.User{
		Email: email,
	})

	if err == sql.ErrNoRows {
		return 0, nil
	}
	if err != nil {
		return 0, err
	}

	return user.ID, nil
}
