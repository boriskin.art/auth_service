package domain

type User struct {
	ID 		 int
	Email 	 string
	Password string
}

type UserUsecase interface {
	SignUp(email, password string) (int, *AuthError)
	SignIn(email, password string) (int, *AuthError)
	CheckUserExistence(email string) (int, error)
}

type UserRepository interface {
	Create(user User) (int, error)
	Get(user User) (*User, error)
}