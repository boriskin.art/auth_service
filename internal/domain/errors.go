package domain

import "fmt"

type AuthError struct {
	StatusCode   int
	ErrorMessage string
}

func NewAuthError(statusCode int, errorMessage string) *AuthError {
	return &AuthError{
		StatusCode:   statusCode,
		ErrorMessage: errorMessage,
	}
}

func (ae *AuthError) Error() string {
	return fmt.Sprintf("[Status code: %v] Error: %v\n", ae.StatusCode, ae.ErrorMessage)
}

var (
	UserDoesNotExist      = NewAuthError(100, "user doesn't exist")
	UserAlreadyExist      = NewAuthError(101, "user already exist")
	UserDataIncorrect     = NewAuthError(102, "email or password incorrect")

	InputDataParseError   = NewAuthError(200, "input data parse error")
	// 201 for govalidator

	// 300 db errors
)
