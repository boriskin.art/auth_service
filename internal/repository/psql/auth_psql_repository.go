package psql

import (
	"avito_service/internal/domain"
	"database/sql"
)

type UserPsqlRepository struct {
	DB *sql.DB
}

func NewUserPsqlRepository(db *sql.DB) *UserPsqlRepository {
	return &UserPsqlRepository{DB: db}
}

func (ur *UserPsqlRepository) Create(user domain.User) (int, error) {
	var userID int
	err := ur.DB.QueryRow("INSERT INTO users (email, password) VALUES ($1, $2) RETURNING id;",
		user.Email,
		user.Password).Scan(&userID)

	if err != nil {
		return 0, err
	}

	return userID, nil
}

func (ur *UserPsqlRepository) Get(user domain.User) (*domain.User, error) {
	u := &domain.User{}

	err := ur.DB.QueryRow(`SELECT id, email, password FROM users WHERE email=$1`,
		user.Email).
		Scan(&u.ID, &u.Email, &u.Password)


	if err != nil {
		return nil, err
	}

	return u, nil
}