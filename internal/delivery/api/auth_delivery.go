package api

import (
	"avito_service/internal/domain"
	"encoding/json"
	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"
	"net/http"
)

type UserHandler struct {
	userUsecase domain.UserUsecase
}

func NewUserHandler(router *mux.Router, userUsecase domain.UserUsecase) {
	handler := UserHandler{userUsecase: userUsecase}

	router.HandleFunc("/sign-up", handler.SignUp).Methods("POST")
	router.HandleFunc("/sign-in", handler.SignIn).Methods("POST")
}

func (uh *UserHandler) SignUp(w http.ResponseWriter, r *http.Request) {
	signUpInput := SignUpInput{}

	err := json.NewDecoder(r.Body).Decode(&signUpInput)
	if err != nil {
		NewErrorResponse(w, domain.InputDataParseError)
		return
	}

	_, err = govalidator.ValidateStruct(signUpInput)
	if err != nil {
		NewErrorResponse(w, domain.NewAuthError(201, "Input data doesn't valid: " + err.Error()))
		return
	}

	userID, e := uh.userUsecase.SignUp(signUpInput.Email, signUpInput.Password)
	if e != nil {
		NewErrorResponse(w, e)
		return
	}

	NewSuccessResponse(w, AuthResponse{UserID: userID})
}

func (uh *UserHandler) SignIn(w http.ResponseWriter, r *http.Request) {
	signInInput := SignInInput{}

	err := json.NewDecoder(r.Body).Decode(&signInInput)
	if err != nil {
		NewErrorResponse(w, domain.InputDataParseError)
		return
	}

	_, err = govalidator.ValidateStruct(signInInput)
	if err != nil {
		NewErrorResponse(w, domain.NewAuthError(201, "Input data doesn't valid: " + err.Error()))
		return
	}

	userID, e := uh.userUsecase.SignIn(signInInput.Email, signInInput.Password)
	if e != nil {
		NewErrorResponse(w, e)
		return
	}

	NewSuccessResponse(w, AuthResponse{UserID: userID})
}