package api

import (
	"avito_service/internal/domain"
	"encoding/json"
	"net/http"
)

type SignInInput struct {
	Email    string `json:"email" valid:"required,email"`
	Password string `json:"password" valid:"required"`
}

type SignUpInput struct {
	Email          string `json:"email" valid:"required,email"`
	Password       string `json:"password" valid:"required"`
}

type SuccessResponse struct {
	StatusCode int         `json:"status_code"`
	Response   interface{} `json:"response"`
}

type AuthResponse struct {
	UserID int `json:"user_id"`
}

type ErrorResponse struct {
	StatusCode   int    `json:"status_code"`
	ErrorMessage string `json:"error_message"`
}


func NewSuccessResponse(w http.ResponseWriter, response interface{}) {
	w.Header().Set("Content-Type", "application/json")

	err := json.NewEncoder(w).Encode(SuccessResponse{
		StatusCode:   0,
		Response: response,
	})

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func NewErrorResponse(w http.ResponseWriter, authError *domain.AuthError) {
	w.Header().Set("Content-Type", "application/json")

	err := json.NewEncoder(w).Encode(ErrorResponse{
		StatusCode:   authError.StatusCode,
		ErrorMessage: authError.ErrorMessage,
	})

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}