package auth_service

import (
	"avito_service/config"
	"avito_service/internal/delivery/api"
	"avito_service/internal/repository/psql"
	"avito_service/internal/usecase"
	"avito_service/pkg/db_connections"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"net/http"
	"time"
)

func Run() {
	config.InitConfig()

	router := mux.NewRouter()

	userRepository := psql.NewUserPsqlRepository(db_connections.GetPostgres())
	userUsecase := usecase.NewUserUsecase(userRepository)
	api.NewUserHandler(router, userUsecase)

	server := http.Server{
		Addr:         viper.GetString("port"),
		Handler:      router,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
	}

	logrus.Info("Starting server...")

	if err := server.ListenAndServe(); err != nil {
		logrus.Fatal("Server error: ", err)
	}
}