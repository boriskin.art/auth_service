package passwords

import (
	"crypto/sha1"
	"fmt"
	"golang.org/x/crypto/pbkdf2"
)

func GeneratePasswordHash(salt, password string) string {
	hashedPass := pbkdf2.Key([]byte(password), []byte(salt), 4096, 32, sha1.New)
	return salt + fmt.Sprintf("%x", hashedPass)
}

func CheckPasswordHash(password, passwordHash string) bool {
	salt := passwordHash[0:16]
	userPasswordHash := GeneratePasswordHash(fmt.Sprintf("%s", salt), password)
	return userPasswordHash == passwordHash
}